(*xが割られる数 yが割る数*)
let rec inner x y =
  if y = 21 then true
  else if x mod y = 0 then inner x (y + 1)
  else false

let rec outer x =
  if inner x 2 then x
  else outer (x + 1)

let () = Printf.printf "%d\n" (outer 21)
