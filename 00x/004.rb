def is_palindrome s
  s == s.reverse
end

ans = 0

999.downto(1) do |x|
  999.downto(1) do |y|
    tmp = x * y
    next unless is_palindrome(tmp.to_s)
    ans = tmp if ans < tmp
  end
end

puts ans
