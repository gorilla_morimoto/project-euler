let sum_of_squares x =
  let rec aux i tmp =
    if i = 0 then tmp
    else aux (i - 1) (tmp + i * i)
  in
  aux x 0

let square_of_sum x =
  let rec aux i tmp = 
    if i = 0 then tmp * tmp
    else aux (i - 1) (tmp + i)
  in
  aux x 0

let solve x = square_of_sum x - sum_of_squares x

let () = solve 100 |> Printf.printf "%d\n"