let is_palindrome x =
  let s = string_of_int x in
  let len = String.length s in
  (*このアルゴリズムはなんかのソートでやった*)
  let rec aux i j =
    if j < i then true
    else if s.[i] = s.[j] then aux (i + 1) (j - 1)
    else false
  in
  aux 0 (len - 1)

(*palindromeが見つからないときは無限ループになる*)
(*y=1で止めればいいけど美しくない、ありえないので書きません*)
let rec inner x y =
  let tmp = (x * y) in
  if is_palindrome tmp then tmp else inner x (y - 1)

(*outerでは外側の変数xだけをデクリメントする*)
(*innerを呼び出すときにはinnerの第二引数に999（初期値）を渡す*)
let rec outer x tmp =
  if x = 1 then tmp
  else
    match inner x 999 with
    | a when tmp < a -> outer (x - 1) a
    | a -> outer (x - 1) tmp

let () = outer 999 min_int |> Printf.printf "%d\n"