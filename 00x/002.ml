let rec f pre next sum =
  match pre with
  | x when x > 4000000 -> sum
  | x when x mod 2 = 0 -> f next (pre + next) (sum + pre)
  | _ -> f next (pre + next) sum

let () = Printf.printf "%d\n" (f 1 2 0)