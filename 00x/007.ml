let prime nth =
  let rec aux i x ls =
    if i = nth then List.hd ls
    else if List.exists (fun p -> x mod p = 0) ls then aux i (x + 1) ls
    else aux (i + 1) (x + 1) (x :: ls)
  in
  aux 1 3 [2]

let () = prime 10001 |> Printf.printf "%d\n"