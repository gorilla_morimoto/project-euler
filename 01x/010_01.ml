let primes n =
  let rec aux i ls =
    if i > n then ls
    else if List.exists (fun x -> i mod x = 0) ls then aux (i + 2) ls
    else aux (i + 2) (i :: ls)
  in
  aux 3 [2]


let ()  = primes 10000 |> List.fold_left (+) 0 |> Printf.printf "%d\n"