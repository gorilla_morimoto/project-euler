let h = Hashtbl.create 100
let () = Hashtbl.add h 1 1 ; Hashtbl.add h 2 3

let rec nums i =
  if Hashtbl.mem h i then Hashtbl.find h i
  else
    let () = Hashtbl.add h i (nums (i - 1) + i) in Hashtbl.find h i

let divisors n = 
  let rec aux i now sum ls =
    if now = 1 then (List.fold_left ( * ) 1 ls) * (sum + 1)
    else if now mod i = 0 then aux i (now / i) (sum + 1) ls
    else aux (i + 1) now 0 (match sum with 0 -> ls | x -> (x+1)::ls)
  in
  aux 2 n 0 []

let rec solve i =
  let tmp = nums i in
  if (divisors tmp) > 500 then tmp
  else solve (i + 1)

let () = solve 1 |> Printf.printf "%d\n"