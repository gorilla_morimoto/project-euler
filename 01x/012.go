package main

import "fmt"

var (
	m = make(map[int]int)
)

func main() {
	m[1] = 1
	m[2] = 3

	i := 1
	for divisors(num(i)) <= 500 {
		i++
	}
	fmt.Println(num(i))
}

func num(i int) int {
	v, ok := m[i]
	if ok {
		return v
	}

	m[i] = num(i-1) + i
	return m[i]
}

func divisors(n int) int {
	var count int
	ls := []int{}
	for i := 2; n != 1; i++ {
		count = 0
		for n%i == 0 {
			n /= i
			count++
		}
		ls = append(ls, count)
	}

	ret := 1
	for _, v := range ls {
		ret *= (v + 1)
	}

	return ret
}
