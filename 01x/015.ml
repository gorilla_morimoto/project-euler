open Hashtbl

let n = 20
let h = create n

let solve x y =
  if x = 0 || y = 0 then 1
  else find h (x-1, y) + find h (x, y-1)

let () = 
  for i = 0 to n do
    for j = 0 to n do
      solve i j |> add h (i, j)
    done ;
  done ;

  find h (n, n) |>  Printf.printf "%d\n"