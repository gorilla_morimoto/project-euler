n = 20

@h = {}

def solve(x, y)
  if x == 1 || y == 1
    1
  else
    @h[[x - 1, y]] + @h[[x, y - 1]]
  end
end

1.upto(n + 1) do |i|
  1.upto(n + 1) do |j|
    routes = solve(i, j)
    @h[[i, j]] = routes
  end
end

res = @h[[n + 1, n + 1]]
p res
