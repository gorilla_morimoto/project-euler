let calc n =
  let rec aux i n' =
    if n' = 1 then (n, i)
    else if n' mod 2 = 1 then aux (i + 1) (3 * n' + 1)
    else aux (i + 1) (n' / 2)
  in
  aux 1 n

let solve () =
  let rec aux n tmp =
    if n >= 1000000 then tmp
    else
      let tmpn', tmpi' = calc n
      and tmpn, tmpi = tmp in
      if tmpi < tmpi' then aux (n + 1) (tmpn', tmpi')
      else aux (n + 1) tmp

  in
  aux 2 (0, 0)

let () = fst (solve ()) |> Printf.printf "%d\n"